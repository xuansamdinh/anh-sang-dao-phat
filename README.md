# README #

**Đường Vào Ánh Sáng Đạo Phật**
===

**TỊNH MẶC**

Vừa đây, mình mạn phép chép lại quyển sách nhỏ, tựa đề **"Đường Vào Ánh Sáng Đạo Phật"**, của tác giả **Tịnh Mặc**. Hiện tại, cũng đã hoàn thành, xuất bản thành định dạng pdf, dễ dàng xem và in ấn với điều kiện hiện nay.

Do nhu cầu tìm đọc các tài liệu bước đầu cho người học Phật, lang thang internet, mình đã tìm được quyển sách kia, tại địa chỉ: authentique.free.fr/duong-vao-anh-sang-dao-phat.htm

Quyển sách nhỏ này được xuất bản cũng đã khá lâu, từ 1958, nên tại địa chỉ trên, chỉ có lại bản scan của sách, nhưng với chất lượng khá kém, khó theo dõi.

Chính vì vậy, do nhu cầu cá nhân, cũng như muốn dễ dàng phổ biến hơn cho những người có mong muốn, nhu cầu được tìm hiểu tương tự. Mình đã tự ý chép lại toàn bộ quyển sách, với nội dung và hình thức gần như copy 100%. Mình không muốn thay đổi bất cứ gì, hình thức, lẫn nội dung. Chỉ đơn giản mình sao chép, và phát hành lại dưới dạng ebook thường, mọi người sẽ dễ thao tác, theo dõi, và in ấn hơn. Các lưu ý, đoạn đầu sách mình cũng có đôi dòng.

Những ai quan tâm, có thể theo dõi và đóng góp, hoặc ý kiến tại đây.

Về bản hoàn thành, tại mục [Downloads](https://bitbucket.org/xuansamdinh/anh-sang-dao-phat/downloads), mọi người có thể tải về bản xem được, bao gồm 2 bản: đều là A5, phông chữ cỡ 11pt:

- Bản 2 mặt, dùng để in ra quyển sách 2 mặt như sách thường.

- Bản 1 mặt, dùng để đọc trên máy, hoặc in 1 mặt/trang. (oneside.pdf)

Trên đây là vài dòng ngắn để giới thiệu. Rất vui nếu giúp mọi người có thêm nhu liệu cho những ai đang có nhu cầu tìm hiểu Đạo Phật.

## Downloads

Tải xuống ebook này tại mục [Downloads](https://bitbucket.org/xuansamdinh/anh-sang-dao-phat/downloads)